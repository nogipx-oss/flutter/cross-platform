# Cross Platform

The Cross Platform provides mixins to simple decide which widget should be built.
For example, we need some widget looks in one way on devices and other way on web.
Or we need to include/exclude (svg, hover, ...) some widgets on web platform.

## Usage

#### PlatformSplitMixin 
Allows to split widget into 'device' and 'web' variants

```dart
class PlatformSplitted 
  extends StatelessWidget 
  with PlatformSplitMixin {

  @override
  Widget build(BuildContext context) {
    // You can pass any data to each variant builder

    // will return green container with platform specific text
    return platformVariant(context, data: Colors.green);
  }

  @override
  Widget device(context, {data}) {
    return Container(
      color: data as Color,
      child: Center(
        child: Text("I'm device variant")
      ),
    );
  }

  @override
  Widget web(context, {data}) {
    final color = data as Color;
    return Container(
      color: color,
      child: Center(
        child: Text("I'm web variant")
      ),
    );
  }
}
```

---
#### PlatformSplitSingletonMixin

Similar to previous mixin but initialize variants only once. Useful, for example, to override navigation.

```dart
class MyApplication
  extends StatelessWidget
  with PlatformSplitSingletonMixin {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MyApplication",
      builder: (context, child) {

        // Initialise singletons outside widgets
        // So they will not rebuild
        initPlatforms(context);
        return platformVariant;
      },
    );
  }

  @override
  Navigator web(context) {
    return Navigator();
  }

  @override
  Navigator device(context) {
    return Navigator();
  }
}
```



