## [0.1.3] - 19 April 2020

* Rename platform mixins to PlatformSplitMixin and PlatformSplitSingletonMixin
* Make mixins available to work with any widget
* Add README

## [0.1.1] - 17 April 2020

* Separate navigators by screen type and platform


## [0.1.0] - 08.04.2020

* Initial mixin add
