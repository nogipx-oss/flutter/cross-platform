import 'package:flutter/foundation.dart';

/// A function to provide an instance of a controller
typedef FactoryBuilder<T> = T Function();

/// Provides an instance of a [Controller]
class Factory<T> {
  String _name;
  final bool isSingleton;
  final FactoryBuilder<T> factory;
  T _singleton;

  Factory({this.isSingleton = true, @required this.factory})
      : assert(factory != null) {
    _name = '$T';
  }

  String get name => _name;
  T get singleton => _singleton;
  bool get hasSingleton => _singleton != null;

  void initializeSingleton() => _singleton = factory();

  @override
  String toString() {
    return 'ControllerFactory{_name: $_name, factory: $factory}';
  }
}
