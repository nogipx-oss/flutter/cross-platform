import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/widgets.dart';
import 'package:flutter_cross_platform/src/factory.dart';
import 'package:responsive_builder/responsive_builder.dart';

mixin PlatformSplitSingletonMixin {
  Factory<Widget> _web;
  Factory<Widget> _device;

  Widget web(context);
  Widget device(context);

  bool _initialized = false;

  Widget get platformVariant {
    assert(_initialized,
        "initNavigation() has not called. Call it in app builder");
    if (kIsWeb)
      return _web.singleton;
    else
      return _device.singleton;
  }

  void initPlatforms(BuildContext context) {
    _web = Factory(factory: () => web(context));
    _device = Factory(factory: () => device(context));

    _web.initializeSingleton();
    _device.initializeSingleton();
    _initialized = true;
  }
}

mixin PlatformSplitMixin {
  Widget web(context, {data});
  Widget device(context, {data});

  Widget platformVariant(BuildContext context, {dynamic data}) {
    if (kIsWeb)
      return web(context, data: data);
    else
      return device(context, data: data);
  }
}

mixin ScreenSplitMixin {
  Widget watch(context) => mobile(context);
  Widget mobile(context);
  Widget tablet(context) => mobile(context);
  Widget desktop(context) => mobile(context);

  Widget screenVariant(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (ctx) => mobile(ctx),
      desktop: (ctx) => desktop(ctx),
      tablet: (ctx) => tablet(ctx),
      watch: (ctx) => watch(ctx),
    );
  }
}
